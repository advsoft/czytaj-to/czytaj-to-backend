#!/bin/bash
set -e
sh -c './wait-for-it.sh db:5432 -t 30'

python manage.py makemigrations corsheaders
yes "yes" | python manage.py migrate

exec "$@"
