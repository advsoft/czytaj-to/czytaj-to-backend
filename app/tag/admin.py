from django.contrib import admin

from tag.models import TagProfile


class TagProfileAdmin(admin.ModelAdmin):
    list_display = ('tag', 'title', 'description', 'background', 'author')
    fields = ('tag', 'moderators', 'title', 'description', 'background', 'author')


admin.site.register(TagProfile, TagProfileAdmin)
