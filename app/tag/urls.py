from django.conf.urls import url

from tag.views import TagProfileView, ObserveTagView, BlockTagView

urlpatterns = [
     url(r'^(?P<tag>\w+)/profile/$', TagProfileView.as_view()),
     url(r'^(?P<tag>\w+)/observe/$', ObserveTagView.as_view()),
     url(r'^(?P<tag>\w+)/block/$', BlockTagView.as_view()),
]