from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from tag.models import TagProfile
from tag.serializers import TagProfileSerializer


class TagProfileView(generics.RetrieveAPIView, ):
    queryset = TagProfile.objects.all()
    serializer_class = TagProfileSerializer

    def get_object(self):
        queryset = self.get_queryset()
        tag = self.kwargs.get('tag')
        return queryset.get(tag__name=tag)


class ObserveTagView(generics.GenericAPIView):
    def post(self, *arg, **kwargs):
        user = self.request.user
        tag = kwargs.get('tag')
        user.observed_tags.add(tag)
        return Response(tag, status.HTTP_201_CREATED)

    def delete(self, *arg, **kwargs):
        user = self.request.user
        tag = self.kwargs.get('tag')
        user.observed_tags.remove(tag)
        return Response(status=status.HTTP_204_NO_CONTENT)


class BlockTagView(generics.GenericAPIView):
    def post(self, *arg, **kwargs):
        user = self.request.user
        tag = kwargs.get('tag')
        user.blocked_tags.add(tag)
        return Response(tag, status.HTTP_201_CREATED)

    def delete(self, *arg, **kwargs):
        user = self.request.user
        tag = kwargs.get('tag')
        user.blocked_tags.remove(tag)
        return Response(status=status.HTTP_204_NO_CONTENT)
