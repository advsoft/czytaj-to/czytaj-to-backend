from rest_framework.test import APITestCase
from requests.auth import HTTPBasicAuth
from taggit.models import Tag

from czytajto.models import User
from microblog.models import Post, Comment
from tag.models import TagProfile


class PostTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')

    def test_get_tag(self):
        post = Post(author=self.user, content='#foo #bar')
        post.save()
        tagProfile = TagProfile(tag=Tag.objects.get(name ='foo'))
        tagProfile.save()
        response = self.client.get('/tag/{}/profile/'.format('foo'))
        self.assertEqual(response.status_code, 200)

class ObservedBlocTagkTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')

    def test_observe_tag(self):
        response = self.client.post('/tag/{}/observe/'.format('foo'))
        self.assertEqual(response.status_code, 201)
        self.user.refresh_from_db()
        self.assertEqual(self.user.observed_tags.first().name, 'foo')

    def test_unobserve_tag(self):
        self.user.observed_tags.add('bar')
        response = self.client.delete('/tag/{}/observe/'.format('bar'))
        self.assertEqual(response.status_code, 204)
        self.user.refresh_from_db()
        self.assertEqual(self.user.observed_tags.count(), 0)

    def test_block_tag(self):
        response = self.client.post('/tag/{}/block/'.format('foo'))
        self.assertEqual(response.status_code, 201)
        self.user.refresh_from_db()
        self.assertEqual(self.user.blocked_tags.first().name, 'foo')

    def test_unblock_tag(self):
        self.user.blocked_tags.add('bar')
        response = self.client.delete('/tag/{}/block/'.format('bar'))
        self.assertEqual(response.status_code, 204)
        self.user.refresh_from_db()
        self.assertEqual(self.user.blocked_tags.count(), 0)