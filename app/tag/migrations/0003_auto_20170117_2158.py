# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-17 21:58
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tag', '0002_auto_20170117_2155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tagprofile',
            name='moderators',
            field=models.ManyToManyField(blank=True, related_name='moderated_tags', to=settings.AUTH_USER_MODEL),
        ),
    ]
