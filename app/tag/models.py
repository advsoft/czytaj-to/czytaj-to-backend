from django.db import models
from taggit.models import Tag

from czytajto.models import User


class TagProfile(models.Model):
    tag = models.OneToOneField(Tag, on_delete=models.CASCADE, related_name='profile')
    moderators = models.ManyToManyField(User, related_name='moderated_tags', blank=True)
    title = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=512, blank=True)
    background = models.ImageField(blank=True, null=True)
    author = models.ForeignKey(User, blank=True, null=True, related_name='owned_tags')
