from rest_framework import serializers

from microblog.serializers import AuthorSerializer
from tag.models import TagProfile


class TagProfileSerializer(serializers.ModelSerializer):
    moderators = AuthorSerializer(many=True, read_only=True)
    author = AuthorSerializer(read_only=True)
    tag = serializers.CharField(source='tag__name', read_only=True)

    class Meta:
        model = TagProfile
        fields = ('tag', 'moderators', 'title', 'description', 'background', 'author')
        read_only_fields = ('tag', 'moderators', 'title', 'description', 'background', 'author')


class AdminTagProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagProfile
        fields = ('tag', 'moderators', 'title', 'description', 'background', 'author')


class ModeratorTagProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagProfile
        fields = ('tag', 'moderators', 'title', 'description', 'background', 'author')
        read_only_fields = ('tag', 'moderators', 'author')
