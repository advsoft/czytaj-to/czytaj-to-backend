from rest_framework import serializers

from czytajto.models import User
from utils.serializers import Base64ImageField
from taggit_serializer.serializers import TagListSerializerField, TaggitSerializer


class UserSerializer(TaggitSerializer, serializers.ModelSerializer):
    avatar = Base64ImageField(required=False)
    observed_tags = TagListSerializerField(required=False)
    blocked_tags = TagListSerializerField(required=False)

    observed_users = serializers.SlugRelatedField(slug_field='username',
                                                  queryset=User.objects.all(),
                                                  many=True,
                                                  required=False)

    blocked_users = serializers.SlugRelatedField(slug_field='username',
                                                 queryset=User.objects.all(),
                                                 many=True,
                                                 required=False)

    observed_by = serializers.SlugRelatedField(slug_field='username',
                                               queryset=User.objects.all(),
                                               many=True,
                                               required=False)

    class Meta:
        model = User
        fields = ('id',
                  'username',
                  'email',
                  'password',
                  'avatar',
                  'observed_tags',
                  'blocked_tags',
                  'observed_users',
                  'observed_by',
                  'blocked_users')
        read_only_fields = ('id',)
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        user.save()
        return user


class UpdateUserSerializer(serializers.ModelSerializer):
    avatar = Base64ImageField(required=False)
    observed_tags = TagListSerializerField(required=False)
    blocked_tags = TagListSerializerField(required=False)
    observed_users = serializers.SlugRelatedField(slug_field='username',
                                                  queryset=User.objects.all(),
                                                  many=True,
                                                  required=False)

    observed_by = serializers.SlugRelatedField(slug_field='username',
                                               queryset=User.objects.all(),
                                               many=True,
                                               required=False)

    blocked_users = serializers.SlugRelatedField(slug_field='username',
                                                 queryset=User.objects.all(),
                                                 many=True,
                                                 required=False)

    class Meta:
        model = User
        fields = ('id',
                  'username',
                  'email',
                  'password',
                  'avatar',
                  'observed_tags',
                  'blocked_tags',
                  'observed_users',
                  'observed_by',
                  'blocked_users')
        read_only_fields = ('id', 'username',)
        extra_kwargs = {'password': {'write_only': True}}

    def update(self, instance, validated_data):
        for key, value in validated_data.items():
            if key == 'password':
                instance.set_password(value)
                continue
            setattr(instance, key, value)
        instance.save()
        return instance


class UserProfileSerializer(TaggitSerializer, serializers.ModelSerializer):
    avatar = Base64ImageField(required=False)
    observed_users = serializers.SlugRelatedField(slug_field='username',
                                                  queryset=User.objects.all(),
                                                  many=True,
                                                  required=False)

    observed_by = serializers.SlugRelatedField(slug_field='username',
                                               queryset=User.objects.all(),
                                               many=True,
                                               required=False)

    class Meta:
        model = User
        fields = ('id',
                  'username',
                  'avatar',
                  'observed_users',
                  'observed_by')
        read_only_fields = ('id',)

