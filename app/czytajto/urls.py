"""czytajto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from czytajto.views import UserView, ObserveUserView, BlockUserView, UserProfileView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tag/', include('tag.urls', namespace='tags')),
    url(r'^microblog/', include('microblog.urls', namespace='microblog')),
    url(r'^oauth2/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^docs/', include('django_rest_swagger_docstring.urls', namespace='docs')),
    url(r'^user/$', UserView.as_view()),

    url(r'^user/(?P<username>\w+)/$', UserProfileView.as_view()),
    url(r'^user/(?P<username>\w+)/observe/$', ObserveUserView.as_view()),
    url(r'^user/(?P<username>\w+)/block/$', BlockUserView.as_view()),
]
