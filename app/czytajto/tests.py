from rest_framework.test import APITestCase
from requests.auth import HTTPBasicAuth

from czytajto.models import User
from microblog.models import Post, Comment


class UserCRUDTests(APITestCase):
    # def setUp(self):
    # self.user = User.objects.create_user(username='root', email='root@root.com', password='root')
    #  self.client.login(username='root', password='root')

    def test_create_user(self):
        json = {
            'username': 'johniak',
            'email': 'johniak@johniak.com',
            'password': 'test',
            'avatar': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAACuklEQVRoge2YwUsUYRiHn1nBVkVNiaVTkuK6dexidYi9ybYEK0Q3vXpK6N7NY4kU1iUP0cHTRhD4H3gUuiREoqakCCu5oO2u68z8OqwLspU732yzazAPvLf33Xeenfebb76BkJCQkJCQkJALhxXUDwuGgRvAIHAJsIEt4LMFX4Lq+08QxAWzgg2BzoldwQtVJC8OSiavCt4K3DoCtWEL5gV9rXYgkUgknb09WyMjJgK18V0w2kqPB0BxbnbWdTc2XHV0NCJTEtxvhcQ9oAgI0Nbq6okWFuwGRCT4KbjbTIkYkKtKAIrFYjrJ511lMqbrpDZ2BFeaJbJwVqIamUxGyuddDQw0IiLBm2ZIxKnsCb+JAPqYzTruyoqjtrZGRE5U6RMoz/8mASgSiejH9rbtzsw0ul7mgpSwgO3zRADF43G5h4eukslG10pgbx6JehLVmJ6elnI5V/39jcgMByXyyKsIoJXlZVtLS42M2MOgRJ6YiESjURVzOUdTU45PkcdBiTw1EQE0Pj4u5+DAlR/W19e8XljEUOTQMJ+uri6OIhHXtA6AwcF5r6mmInuG+UxOTjo9PT1tpnWn7Pqsq8sQBmPV3d2tUqnka6pO8bwpmt6RdWDHa3IqlaJUKjmGPapsWpb11WuyqQjAO6+JExMTTm9vr58eAB981nlmCChTZ6w6OztVLBb9jtSJpOsmF+Xn31oHXtVLGhsbo1wu+x2rl5ZlbfqsNaIP2OScO5LNZm3Xdf3sH98kXW6GRJVRoPAniWg0qkKh4GekCpJuN1OiSgo4rhVJp9PK5/O2oURRUkvO7FVuAWtnRRYXF03HalfSnVZKVOkDXgN2e3u7jo6OvAqUJc2ryWvCC4l0Ov1+f3//uI7AtqRnkgb+VePgvv1KN6kcxK5R+fZ7TOV0+alZj9aQkJCQkJCQ/5VfBK6JrHVtkdcAAAAASUVORK5CYII='
        }
        response = self.client.post('/user/', json, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(User.objects.filter(username=json['username']).count(), 1)

    def test_update_user_valid(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')
        json = {
            'password': 'test123',
            'avatar': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAACuklEQVRoge2YwUsUYRiHn1nBVkVNiaVTkuK6dexidYi9ybYEK0Q3vXpK6N7NY4kU1iUP0cHTRhD4H3gUuiREoqakCCu5oO2u68z8OqwLspU732yzazAPvLf33Xeenfebb76BkJCQkJCQkJALhxXUDwuGgRvAIHAJsIEt4LMFX4Lq+08QxAWzgg2BzoldwQtVJC8OSiavCt4K3DoCtWEL5gV9rXYgkUgknb09WyMjJgK18V0w2kqPB0BxbnbWdTc2XHV0NCJTEtxvhcQ9oAgI0Nbq6okWFuwGRCT4KbjbTIkYkKtKAIrFYjrJ511lMqbrpDZ2BFeaJbJwVqIamUxGyuddDQw0IiLBm2ZIxKnsCb+JAPqYzTruyoqjtrZGRE5U6RMoz/8mASgSiejH9rbtzsw0ul7mgpSwgO3zRADF43G5h4eukslG10pgbx6JehLVmJ6elnI5V/39jcgMByXyyKsIoJXlZVtLS42M2MOgRJ6YiESjURVzOUdTU45PkcdBiTw1EQE0Pj4u5+DAlR/W19e8XljEUOTQMJ+uri6OIhHXtA6AwcF5r6mmInuG+UxOTjo9PT1tpnWn7Pqsq8sQBmPV3d2tUqnka6pO8bwpmt6RdWDHa3IqlaJUKjmGPapsWpb11WuyqQjAO6+JExMTTm9vr58eAB981nlmCChTZ6w6OztVLBb9jtSJpOsmF+Xn31oHXtVLGhsbo1wu+x2rl5ZlbfqsNaIP2OScO5LNZm3Xdf3sH98kXW6GRJVRoPAniWg0qkKh4GekCpJuN1OiSgo4rhVJp9PK5/O2oURRUkvO7FVuAWtnRRYXF03HalfSnVZKVOkDXgN2e3u7jo6OvAqUJc2ryWvCC4l0Ov1+f3//uI7AtqRnkgb+VePgvv1KN6kcxK5R+fZ7TOV0+alZj9aQkJCQkJCQ/5VfBK6JrHVtkdcAAAAASUVORK5CYII='
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.client.login(username='root', password='root'))
        self.assertTrue(self.client.login(username='root', password='test123'))

    def test_update_email(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')
        json = {
            'email': 'j@j.com'
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['email'], json['email'])

    def test_update_username(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')
        json = {
            'username': 'test'
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['username'], user.username)

    def test_delete_user(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')
        response = self.client.delete('/user/', format='json')
        self.assertEqual(response.status_code, 204)
        self.assertFalse(User.objects.filter(pk=user.pk).exists())

    def test_update_observed_tags(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')
        json = {
            'observed_tags': ['test']
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertIn('test', response.data['observed_tags'])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['username'], user.username)

    def test_update_blocked_tags(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')
        json = {
            'blocked_tags': ['foo']
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertIn('foo', response.data['blocked_tags'])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['username'], user.username)

    def test_update_observed_users(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        user1 = User.objects.create_user(username='foo', email='foo@root.com', password='foo')
        self.client.login(username='root', password='root')
        json = {
            'observed_users': [user1.username]
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertIn(user1.username, response.data['observed_users'])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['username'], user.username)

    def test_update_observed_users_nonexisting(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')
        json = {
            'observed_users': ['BAR']
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertEqual(response.status_code, 400)

    def test_update_blocked_users(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        user1 = User.objects.create_user(username='foo', email='foo@root.com', password='foo')
        self.client.login(username='root', password='root')
        json = {
            'blocked_users': [user1.username]
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertIn(user1.username, response.data['blocked_users'])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['username'], user.username)

    def test_update_blocked_users_nonexisting(self):
        user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')
        json = {
            'blocked_users': ['BAR']
        }
        response = self.client.patch('/user/', json, format='json')
        self.assertEqual(response.status_code, 400)


class ObservedBlockUserTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.observing_user = User.objects.create_user(username='foo_bar', email='foo_bar@foo_bar.com', password='root')
        self.client.login(username='root', password='root')

    def test_observe_user(self):
        response = self.client.post('/user/{}/observe/'.format('foo_bar'))
        self.assertEqual(response.status_code, 201)
        self.user.refresh_from_db()
        self.assertEqual(self.user.observed_users.first().username, 'foo_bar')

    def test_unobserve_user(self):
        self.user.observed_users.add(self.observing_user)
        response = self.client.delete('/user/{}/observe/'.format('foo_bar'))
        self.assertEqual(response.status_code, 204)
        self.user.refresh_from_db()
        self.assertEqual(self.user.observed_users.count(), 0)

    def test_block_user(self):
        response = self.client.post('/user/{}/block/'.format('foo_bar'))
        self.assertEqual(response.status_code, 201)
        self.user.refresh_from_db()
        self.assertEqual(self.user.blocked_users.first().username, 'foo_bar')

    def test_unblock_user(self):
        self.user.blocked_users.add(self.observing_user)
        response = self.client.delete('/user/{}/block/'.format('foo_bar'))
        self.assertEqual(response.status_code, 204)
        self.user.refresh_from_db()
        self.assertEqual(self.user.blocked_users.count(), 0)


class UserProfileTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.other_user = User.objects.create_user(username='foo_bar', email='foo_bar@foo_bar.com', password='root')
        self.client.login(username='root', password='root')

    def test_get_user_profile(self):
        response = self.client.get('/user/{}/'.format('foo_bar'))
        self.assertEqual(response.status_code, 200)
