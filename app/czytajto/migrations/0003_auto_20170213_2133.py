# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-02-13 21:33
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('czytajto', '0002_user_avatar'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlockedTags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ObservedTags',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='user',
            name='observed_users',
            field=models.ManyToManyField(blank=True, related_name='observed_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=255, unique=True, verbose_name='email address'),
        ),
        migrations.AddField(
            model_name='observedtags',
            name='content_object',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='observedtags',
            name='tag',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='czytajto_observedtags_items', to='taggit.Tag'),
        ),
        migrations.AddField(
            model_name='blockedtags',
            name='content_object',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='blockedtags',
            name='tag',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='czytajto_blockedtags_items', to='taggit.Tag'),
        ),
        migrations.AddField(
            model_name='user',
            name='blocked_tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='czytajto.BlockedTags', to='taggit.Tag', verbose_name='Tags'),
        ),
        migrations.AddField(
            model_name='user',
            name='observed_tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='czytajto.ObservedTags', to='taggit.Tag', verbose_name='Tags'),
        ),
    ]
