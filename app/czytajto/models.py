from django.contrib.auth.models import AbstractUser
from django.db import models
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase


class BlockedTags(TaggedItemBase):
    content_object = models.ForeignKey('User')


class ObservedTags(TaggedItemBase):
    content_object = models.ForeignKey('User')


class User(AbstractUser):
    avatar = models.ImageField(null=True, blank=True)

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    blocked_tags = TaggableManager(through=BlockedTags, related_name='blocked_by')
    observed_tags = TaggableManager(through=ObservedTags, related_name='observed_by')
    observed_users = models.ManyToManyField('czytajto.User', related_name='observed_by', blank=True)
    blocked_users = models.ManyToManyField('czytajto.User', related_name='blocked_by', blank=True)

    @property
    def is_admin(self):
        return self.groups.filter(name='admin').count() > 0

    REQUIRED_FIELDS = ['email']

