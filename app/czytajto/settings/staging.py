from .base import *

ALLOWED_HOSTS = [
    'api.czytajto.advsoft.org'
]

STATIC_ROOT = os.path.join('/', 'static')
STATIC_URL = '/static/'
MEDIA_ROOT = os.path.join('/', 'media')
MEDIA_URL = '/media/'

RAVEN_CONFIG = {
    'dsn': 'http://fd7ec732abd241d9b1615ef5d666432f:eda4e0e52e5d48bda8d169359759df86@sentry.uran.advsoft.org/2',
}