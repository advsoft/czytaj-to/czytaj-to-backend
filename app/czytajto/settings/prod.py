from .base import *


ALLOWED_HOSTS = [
    'czytaj.to'
]

STATIC_ROOT = os.path.join('/', 'static')
STATIC_URL = '/static/'
MEDIA_ROOT = os.path.join('/', 'media')
MEDIA_URL = '/media/'

RAVEN_CONFIG = {
    'dsn': 'http://58a711f5384140958bf2e30bf038d819:1e4b8d4974bb45a497052d17e078866b@sentry.uran.advsoft.org/3',
}