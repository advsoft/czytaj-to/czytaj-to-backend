from .base import *


DEBUG = True

AUTH_PASSWORD_VALIDATORS = [
]

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'veeu': {
            'type': 'oauth2',
            'flow': 'password',
            'tokenUrl': 'http://localhost:8000/oauth2/token/',
            'authorizationUrl':'http://localhost:8000/oauth2/token/',
            'scopes': {
                'write:all': 'Write all',
                'read:all': 'Read all',
            }
        }
    },
}
