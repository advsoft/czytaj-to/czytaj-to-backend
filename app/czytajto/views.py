from rest_framework import generics
from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from czytajto.models import User
from czytajto.serializers import UserSerializer, UpdateUserSerializer, UserProfileSerializer


class UserView(generics.RetrieveAPIView,
               generics.CreateAPIView,
               generics.UpdateAPIView,
               generics.DestroyAPIView):
    queryset = User.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'PUT' or self.request.method == 'PATCH':
            return UpdateUserSerializer
        return UserSerializer

    def get_object(self):
        return self.request.user


class UserProfileView(generics.RetrieveAPIView):
    serializer_class = UserProfileSerializer
    def get_object(self):
        username = self.kwargs.get('username')
        user = get_object_or_404(User.objects, username=username)
        return user


class ObserveUserView(generics.GenericAPIView):
    def post(self, *arg, **kwargs):
        user = self.request.user
        username = self.kwargs.get('username')
        user.observed_users.add(User.objects.get(username=username))
        return Response(UserSerializer(instance=user).data, status.HTTP_201_CREATED)

    def delete(self, *arg, **kwargs):
        user = self.request.user
        username = self.kwargs.get('username')
        user.observed_users.remove(User.objects.get(username=username))
        return Response(status=status.HTTP_204_NO_CONTENT)


class BlockUserView(generics.GenericAPIView):
    def post(self, *arg, **kwargs):
        user = self.request.user
        username = self.kwargs.get('username')
        user.blocked_users.add(User.objects.get(username=username))
        return Response(UserSerializer(instance=user).data, status.HTTP_201_CREATED)

    def delete(self, *arg, **kwargs):
        user = self.request.user
        username = self.kwargs.get('username')
        user.blocked_users.remove(User.objects.get(username=username))
        return Response(status=status.HTTP_204_NO_CONTENT)
