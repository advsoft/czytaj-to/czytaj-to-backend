var page = require('webpage').create(),
    system = require('system');

if (system.args.length !== 2) {
    console.error('Usage: render.js <some URL>');
    phantom.exit();
}

url = system.args[1];


page.viewportSize = {width: 960, height: 600};
page.paperSize = {width: 960, height: 600};
page.clipRect = {top: 0, left: 0, width: 960, height: 600};
page.onError = function(msg, trace) {

};
page.open(url, function () {
    var base64 = page.renderBase64('JPG');
    console.log(base64);
    phantom.exit();
});