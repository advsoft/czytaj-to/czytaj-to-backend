import re

def get_tags_from_content(content):
    return re.findall(r"#(\w\w+)", content)