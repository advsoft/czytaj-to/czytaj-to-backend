import base64
import subprocess
import uuid

from celery import shared_task
from django.core.files.base import ContentFile

from microblog.models import Post


@shared_task
def render_article_image(post_id):
    post = Post.objects.get(pk=post_id)
    if post.article_url is None:
        return
    proc = subprocess.Popen(['phantomjs', './renderer/render.js', post.article_url], stdout=subprocess.PIPE, )
    output = proc.stdout.read()
    post.article_image = ContentFile(base64.b64decode(output), name='{}.{}'.format(uuid.uuid4(), 'jpg'))
    post.save()
