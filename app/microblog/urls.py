from django.conf.urls import url

from microblog.views import  CommentUpdateDestroyView, PostCommentCreateView, PostCommentListView, \
    PostDestroyUpdateDeleteView, PostCreateView, PostList, CommentToCommentCreateView, PostLikeCreateDestroy, \
    CommentLikeCreateDestroy

urlpatterns = [
    url(r'^post/(?P<pk>\d+)/$', PostDestroyUpdateDeleteView.as_view()),
    url(r'^post/$', PostCreateView.as_view()),
    url(r'^posts/$', PostList.as_view()),
    url(r'^post/(?P<post_pk>\d+)/comment/$', PostCommentCreateView.as_view()),
    url(r'^post/(?P<post_pk>\d+)/comments/$', PostCommentListView.as_view()),
    url(r'^post/(?P<post_pk>\d+)/like/$', PostLikeCreateDestroy.as_view()),
    url(r'^comment/(?P<pk>\d+)/$', CommentUpdateDestroyView.as_view()),
    url(r'^comment/(?P<comment_pk>\d+)/comment/$', CommentToCommentCreateView.as_view()),
    url(r'^comment/(?P<comment_pk>\d+)/like/$', CommentLikeCreateDestroy.as_view()),

]