import django_filters

from microblog.models import Post


class CharInFilter(django_filters.BaseInFilter, django_filters.CharFilter):
    pass


class PostFilter(django_filters.FilterSet):
    date_from = django_filters.IsoDateTimeFilter(name='created', lookup_expr='gte')
    date_to = django_filters.IsoDateTimeFilter(name='created', lookup_expr='lte')
    order_by = django_filters.OrderingFilter(fields=(
        ('created', 'date'),
        ('likes_count_db', 'likes'),
    ), )
    tags = CharInFilter(name='tags', lookup_expr='name__in')
    author = django_filters.CharFilter(name='author__username')

    class Meta:
        model = Post
        fields = ['date_from', 'date_to']
