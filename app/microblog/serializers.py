import base64

from django.core.files.base import ContentFile
from django.db.models import Q
from rest_framework import serializers
from taggit.models import Tag

from czytajto.models import User
from microblog.models import Post, Comment, Like, Media
from tag.models import TagProfile
from utils.helpers import get_tags_from_content
import uuid

from utils.serializers import Base64ImageField


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'avatar',)
        read_only_fields = ('username', 'avatar',)


class LikeSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)

    class Meta:
        model = Like
        fields = ('author',)
        read_only_fields = ('author',)


class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('name',)


class MediaSerializer(serializers.ModelSerializer):
    image = Base64ImageField(required=False)

    class Meta:
        model = Media
        fields = ('image', 'id')
        read_only_fields = ('id',)


class CommentSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)
    likes = LikeSerializer(many=True, read_only=True)
    tags = TagsSerializer(many=True, read_only=True)
    is_liked = serializers.SerializerMethodField('is_liked_method', read_only=True)

    def is_liked_method(self, obj):
        request = self.context.get('request')
        if request.user.is_authenticated():
            return obj.likes_set.filter(author=request.user).count() == 1
        return False

    class Meta:
        model = Comment
        fields = ('id', 'content', 'author', 'created', 'updated', 'likes_count', 'likes', 'tags', 'is_liked',)
        read_only_fields = ('id', 'author', 'created', 'updated', 'likes_count', 'likes', 'tags', 'is_liked',)


class PostCommentSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)
    likes = LikeSerializer(many=True, read_only=True)
    tags = TagsSerializer(many=True, read_only=True)
    comments = CommentSerializer(many=True, read_only=True)
    is_liked = serializers.SerializerMethodField('is_liked_method', read_only=True)

    def is_liked_method(self, obj):
        request = self.context.get('request')
        if request.user.is_authenticated():
            return obj.likes_set.filter(author=request.user).count() == 1
        return False

    class Meta:
        model = Comment
        fields = (
            'id', 'content', 'author', 'created', 'updated', 'likes_count', 'likes', 'tags', 'comments', 'is_liked',)
        read_only_fields = (
            'id', 'author', 'created', 'updated', 'likes_count', 'likes', 'tags', 'comments', 'is_liked',)


class PostSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)
    likes = LikeSerializer(many=True, read_only=True)
    comments = PostCommentSerializer(many=True, read_only=True)
    tags = TagsSerializer(many=True, read_only=True)
    is_liked = serializers.SerializerMethodField('is_liked_method', read_only=True)
    media_set = MediaSerializer(required=False, many=True)
    article_image = Base64ImageField(required=False, read_only=True)

    def is_liked_method(self, obj):
        request = self.context.get('request')
        if request.user.is_authenticated():
            return obj.likes_set.filter(author=request.user).count() == 1
        return False

    def create(self, validated_data):
        media_set = validated_data.pop('media_set', None)
        post = Post(**validated_data)
        post.save()
        if media_set is None:
            return post
        for media in media_set:
            post.media_set.create(**media)
        return post

    def validate_content(self, value):
        request = self.context.get('request')
        user = request.user
        if user.is_superuser or user.is_admin:
            return value
        tags = get_tags_from_content(value)
        authors_tags = TagProfile.objects.filter(
            Q(tag__name__in=tags) &
            Q(author__isnull=False) &
            ~Q(author=request.user))
        if len(authors_tags) > 0:
            raise serializers.ValidationError('You used authors tags: {}'.format(','.join(authors_tags)))
        return value

    class Meta:
        model = Post
        fields = (
            'id', 'content', 'author', 'created', 'updated', 'comments', 'likes_count', 'likes', 'tags', 'is_liked',
            'article_url', 'comments_count', 'media_set', 'article_image')
        read_only_fields = (
            'id', 'author', 'created', 'updated', 'comments', 'likes_count', 'likes', 'tags', 'is_liked',
            'comments_count', 'article_image')


class PostShortSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(read_only=True)
    likes = LikeSerializer(many=True, read_only=True)
    comments = CommentSerializer(many=True, read_only=True, source='best_comments')
    tags = TagsSerializer(many=True, read_only=True)
    is_liked = serializers.SerializerMethodField('is_liked_method', read_only=True)
    media_set = MediaSerializer(required=False, many=True)
    likes_count = serializers.IntegerField(read_only=True)
    article_image = Base64ImageField(required=False, read_only=True)

    def is_liked_method(self, obj):
        request = self.context.get('request')
        if request.user.is_authenticated():
            return obj.likes_set.filter(author=request.user).count() == 1
        return False

    class Meta:
        model = Post
        fields = (
            'id', 'content', 'author', 'created', 'updated', 'comments', 'likes_count', 'likes', 'tags', 'is_liked',
            'article_url', 'comments_count', 'media_set', 'article_image')
        read_only_fields = (
            'id', 'author', 'created', 'updated', 'comments', 'likes_count', 'likes', 'tags', 'is_liked',
            'comments_count', 'article_image')


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('id',)
        read_only_fields = ('id',)
