from unittest.mock import patch

from django.test import override_settings
from rest_framework.test import APITestCase
from requests.auth import HTTPBasicAuth

from czytajto.models import User
from microblog.models import Post, Comment
from microblog import tasks

class PostTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.client.login(username='root', password='root')

    @patch('microblog.tasks.render_article_image.delay')
    def test_create_post(self, delay):
        content = 'Foo bar 123'
        media_set = [{
            'image': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAACuklEQVRoge2YwUsUYRiHn1nBVkVNiaVTkuK6dexidYi9ybYEK0Q3vXpK6N7NY4kU1iUP0cHTRhD4H3gUuiREoqakCCu5oO2u68z8OqwLspU732yzazAPvLf33Xeenfebb76BkJCQkJCQkJALhxXUDwuGgRvAIHAJsIEt4LMFX4Lq+08QxAWzgg2BzoldwQtVJC8OSiavCt4K3DoCtWEL5gV9rXYgkUgknb09WyMjJgK18V0w2kqPB0BxbnbWdTc2XHV0NCJTEtxvhcQ9oAgI0Nbq6okWFuwGRCT4KbjbTIkYkKtKAIrFYjrJ511lMqbrpDZ2BFeaJbJwVqIamUxGyuddDQw0IiLBm2ZIxKnsCb+JAPqYzTruyoqjtrZGRE5U6RMoz/8mASgSiejH9rbtzsw0ul7mgpSwgO3zRADF43G5h4eukslG10pgbx6JehLVmJ6elnI5V/39jcgMByXyyKsIoJXlZVtLS42M2MOgRJ6YiESjURVzOUdTU45PkcdBiTw1EQE0Pj4u5+DAlR/W19e8XljEUOTQMJ+uri6OIhHXtA6AwcF5r6mmInuG+UxOTjo9PT1tpnWn7Pqsq8sQBmPV3d2tUqnka6pO8bwpmt6RdWDHa3IqlaJUKjmGPapsWpb11WuyqQjAO6+JExMTTm9vr58eAB981nlmCChTZ6w6OztVLBb9jtSJpOsmF+Xn31oHXtVLGhsbo1wu+x2rl5ZlbfqsNaIP2OScO5LNZm3Xdf3sH98kXW6GRJVRoPAniWg0qkKh4GekCpJuN1OiSgo4rhVJp9PK5/O2oURRUkvO7FVuAWtnRRYXF03HalfSnVZKVOkDXgN2e3u7jo6OvAqUJc2ryWvCC4l0Ov1+f3//uI7AtqRnkgb+VePgvv1KN6kcxK5R+fZ7TOV0+alZj9aQkJCQkJCQ/5VfBK6JrHVtkdcAAAAASUVORK5CYII='}]
        json = {
            'content': content,
            'article_url': 'http://google.com/',
            'media_set': media_set}
        response = self.client.post('/microblog/post/', json, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['content'], content)
        self.assertEqual(Post.objects.count(), 1)
        self.assertIn('likes_count', response.data)
        tasks.render_article_image.delay.assert_called_once_with(response.data['id'])

    @patch('microblog.tasks.render_article_image.delay')
    def test_save_tags(self, delay):
        content = '#foo #bar'
        json = {'content': content}
        response = self.client.post('/microblog/post/', json, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['content'], content)
        self.assertEqual(len(response.data['tags']), 2)
        tags = [tag['name'] for tag in response.data['tags']]
        self.assertIn('bar', tags)
        self.assertIn('foo', tags)

    @patch('microblog.tasks.render_article_image.delay')
    def test_get_post(self, delay):
        content = 'Foo bar'
        json = {'content': content}
        response = self.client.post('/microblog/post/', json, format='json')
        self.assertEqual(response.status_code, 201)
        post_id = response.data['id']
        response = self.client.get('/microblog/post/{}/'.format(post_id))
        self.assertEqual(response.data['content'], content)
        self.assertIn('likes_count', response.data)

    @patch('microblog.tasks.render_article_image.delay')
    def test_update_post(self, delay):
        content = 'Foo bar'
        json = {'content': content}
        response = self.client.post('/microblog/post/', json, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['content'], content)
        post_id = response.data['id']
        content = 'not foo not bar'
        json = {'content': content}
        response = self.client.put('/microblog/post/{}/'.format(post_id), json, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['content'], content)

    def test_get_posts(self):
        post = Post(author=self.user)
        post.save()
        post = Post(author=self.user)
        post.save()
        response = self.client.get('/microblog/posts/', format='json')
        self.assertEqual(response.status_code, 200)
        results = response.data['results']
        self.assertGreater(len(results), 1)
        self.assertIn('likes_count', results[0])
        self.assertIn('comments_count', results[0])

    def test_get_posts_blocked_tag(self):
        self.user.blocked_tags.add('foo')
        author = User.objects.create_user(username='johniak', email='johniak@johniak.com', password='root')
        post = Post(author=author, content='#foo test')
        post.save()
        post = Post(author=self.user, content='#bar test')
        post.save()
        response = self.client.get('/microblog/posts/', format='json')
        self.assertEqual(response.status_code, 200)
        results = response.data['results']
        self.assertEqual(len(results), 1)
        self.assertIn('likes_count', results[0])
        self.assertIn('comments_count', results[0])

    def test_get_posts_blocked_user(self):
        author = User.objects.create_user(username='johniak', email='johniak@johniak.com', password='root')
        self.user.blocked_users.add(author)
        post = Post(author=author, content='#foo test')
        post.save()
        post = Post(author=author, content='#bar test')
        post.save()
        response = self.client.get('/microblog/posts/', format='json')
        self.assertEqual(response.status_code, 200)
        results = response.data['results']
        self.assertEqual(len(results), 0)

    def test_get_posts_order_by_tag(self):
        post = Post(author=self.user, content='0')
        post.save()
        post = Post(author=self.user, content='1')
        post.save()
        response = self.client.get('/microblog/posts/?order_by=-likes,-date', format='json')
        self.assertEqual(response.status_code, 200)
        results = response.data['results']
        self.assertGreater(len(results), 1)
        first = results[0]
        second = results[1]
        self.assertEqual(first['content'], '1')
        self.assertEqual(second['content'], '0')


class PostCommentTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.post = Post(content='Foo bar', author=self.user)
        self.post.save()
        self.client.login(username='root', password='root')

    def test_create_comment_on_post(self):
        content = 'Foo bar'
        json = {'content': content}
        response = self.client.post('/microblog/post/{}/comment/'.format(self.post.id), json, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['content'], content)

    def test_create_comment_on_comment(self):
        content = 'Foo bar'
        json = {'content': content}
        response = self.client.post('/microblog/post/{}/comment/'.format(self.post.id), json, format='json')
        content = 'Foo bar111'
        json = {'content': content}
        response = self.client.post('/microblog/comment/{}/comment/'.format(response.data['id']), json, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['content'], content)

    def test_save_comments_tags(self):
        content = '#foo #bar'
        json = {'content': content}
        response = self.client.post('/microblog/post/{}/comment/'.format(self.post.id), json, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['content'], content)
        self.assertEqual(len(response.data['tags']), 2)
        tags = [tag['name'] for tag in response.data['tags']]
        self.assertIn('bar', tags)
        self.assertIn('foo', tags)

    def test_get_comments(self):
        comment = Comment(author=self.user, parent=self.post)
        comment.save()
        comment = Comment(author=self.user, parent=self.post)
        comment.save()
        response = self.client.get('/microblog/post/{}/comments/'.format(self.post.id), format='json')
        self.assertEqual(response.status_code, 200)
        self.assertGreater(len(response.data), 1)

    def test_update_comment(self):
        content = 'Foo bar'
        json = {'content': content}
        response = self.client.post('/microblog/post/{}/comment/'.format(self.post.id), json, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['content'], content)
        comment_id = response.data['id']
        content = 'not foo not bar'
        json = {'content': content}
        response = self.client.put('/microblog/comment/{}/'.format(comment_id), json,
                                   format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['content'], content)


class LikeCommentTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='root', email='root@root.com', password='root')
        self.post = Post(content='Foo bar', author=self.user)
        self.post.save()
        self.comment = Comment(content='test', parent=self.post, author=self.user)
        self.comment.save()
        self.client.login(username='root', password='root')

    def test_like_post(self):
        response = self.client.post('/microblog/post/{}/like/'.format(self.post.id))
        self.assertEqual(response.status_code, 201)
        response = self.client.delete('/microblog/post/{}/like/'.format(self.post.id))
        self.assertEqual(response.status_code, 204)
        response = self.client.post('/microblog/post/{}/like/'.format(self.post.id))
        self.assertEqual(response.status_code, 201)
        response = self.client.delete('/microblog/post/{}/like/'.format(self.post.id))
        self.assertEqual(response.status_code, 204)

    def test_like_comment(self):
        response = self.client.post('/microblog/comment/{}/like/'.format(self.comment.id))
        self.assertEqual(response.status_code, 201)
        response = self.client.delete('/microblog/comment/{}/like/'.format(self.comment.id))
        self.assertEqual(response.status_code, 204)
        response = self.client.post('/microblog/comment/{}/like/'.format(self.comment.id))
        self.assertEqual(response.status_code, 201)
        response = self.client.delete('/microblog/comment/{}/like/'.format(self.comment.id))
        self.assertEqual(response.status_code, 204)

    def test_is_liked(self):
        response = self.client.get('/microblog/post/{}/'.format(self.post.id))
        self.assertEqual(response.data['is_liked'], False)
        response = self.client.post('/microblog/post/{}/like/'.format(self.post.id))
        self.assertEqual(response.status_code, 201)
        response = self.client.get('/microblog/post/{}/'.format(self.post.id))
        self.assertEqual(response.data['is_liked'], True)
