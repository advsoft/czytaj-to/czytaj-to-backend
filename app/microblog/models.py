import sys
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Count
from taggit.managers import TaggableManager

from utils.helpers import get_tags_from_content


class PostManager(models.Manager):
    def get_queryset(self):
        return super(PostManager, self).get_queryset() \
            .annotate(likes_count_db=Count('likes_set')) \
            .annotate(comments_count_db=Count('comment_set'))


class Like(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey('czytajto.User')
    limit = models.Q(app_label='microblog', model='post') | models.Q(app_label='microblog', model='comment')
    object_type = models.ForeignKey(ContentType, limit_choices_to=limit)
    object_id = models.PositiveIntegerField()
    object = GenericForeignKey('object_type', 'object_id', )

    class Meta:
        unique_together = ('author', 'object_type', 'object_id')


class Media(models.Model):
    image = models.ImageField(null=True, blank=True)
    limit = models.Q(app_label='microblog', model='post') | models.Q(app_label='microblog', model='comment')
    parent_type = models.ForeignKey(ContentType, limit_choices_to=limit)
    parent_id = models.PositiveIntegerField()
    parent = GenericForeignKey('parent_type', 'parent_id', )


class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    content = models.TextField(max_length=10000)
    tags = TaggableManager()
    author = models.ForeignKey('czytajto.User')
    likes_set = GenericRelation(Like, content_type_field='object_type')
    comment_set = GenericRelation('Comment', content_type_field='parent_type', object_id_field='parent_id')
    article_url = models.URLField(max_length=1000, blank=True, null=True)
    article_image = models.ImageField(null=True, blank=True)
    media_set = GenericRelation('Media', content_type_field='parent_type', object_id_field='parent_id')

    objects = PostManager()

    @property
    def comments(self):
        return self.comment_set.all()

    @property
    def comments_count(self):
        try:
            return self.comments_count_db
        except AttributeError:
            return 0

    def __str__(self):
        return '{}: {}...'.format(self.author.username, self.content[:30])

    @property
    def best_comments(self):
        comments = self.comments.annotate(likes_count_db=Count('likes_set')).order_by('-likes_count_db')[:2]
        return comments

    @property
    def likes_count(self):
        try:
            return self.likes_count_db
        except AttributeError:
            return 0

    @property
    def likes(self):
        return self.likes_set.all()

    def save(self, *args, **kwargs):
        super(Post, self).save(*args, **kwargs)
        self.tags.set(*get_tags_from_content(self.content), clear=True)


class Comment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    content = models.TextField(max_length=10000)
    tags = TaggableManager()
    author = models.ForeignKey('czytajto.User')
    likes_set = GenericRelation(Like, content_type_field='object_type')
    limit = models.Q(app_label='microblog', model='post') | models.Q(app_label='microblog', model='comment')
    parent_type = models.ForeignKey(ContentType, limit_choices_to=limit)
    parent_id = models.PositiveIntegerField()
    parent = GenericForeignKey('parent_type', 'parent_id', )
    comment_set = GenericRelation('Comment', content_type_field='parent_type', object_id_field='parent_id')
    media_set = GenericRelation('Media', content_type_field='parent_type', object_id_field='parent_id')

    @property
    def comments(self):
        return self.comment_set.all()

    @property
    def likes_count(self):
        return self.likes_set.count()

    @property
    def likes(self):
        return self.likes_set.all()

    def clean(self):
        super(Comment, self).clean()
        if type(self.parent) == Comment and type(self.parent.parent) == Comment:
            raise ValidationError('There is only one level of nesting allowed')

    def save(self, *args, **kwargs):
        super(Comment, self).save(*args, **kwargs)
        self.tags.set(*get_tags_from_content(self.content), clear=True)
