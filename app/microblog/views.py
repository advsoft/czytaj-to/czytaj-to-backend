import subprocess

from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated

from microblog.filters import PostFilter
from microblog.models import Post, Comment, Like
from microblog.permisions import IsAuthorOrReadOnly
from microblog.serializers import PostSerializer, PostCommentSerializer, PostShortSerializer, CommentSerializer, \
    LikeSerializer
from microblog.tasks import render_article_image
from utils.helpers import get_tags_from_content


class PostDestroyUpdateDeleteView(generics.DestroyAPIView,
                                  generics.UpdateAPIView,
                                  generics.RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsAuthorOrReadOnly)


class PostCreateView(generics.CreateAPIView, ):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        post = serializer.save(author=self.request.user)
        render_article_image.delay(post.pk)


class PostList(generics.ListAPIView, ):
    queryset = Post.objects.order_by('-created')
    serializer_class = PostShortSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    pagination_class = LimitOffsetPagination
    paginate_by = 20
    filter_class = PostFilter

    def get_queryset(self):
        queryset = Post.objects.order_by('-created')
        user = self.request.user
        if not user.is_authenticated:
            return queryset
        queryset = queryset.filter(~Q(tags__in=user.blocked_tags.all()) & ~Q(author__in=user.blocked_users.all()))
        return queryset


class CommentUpdateDestroyView(generics.DestroyAPIView,
                               generics.UpdateAPIView, ):
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsAuthorOrReadOnly)
    queryset = Comment.objects.all()


class PostCommentCreateView(generics.CreateAPIView, ):
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        post_pk = self.kwargs.get('post_pk')
        post = get_object_or_404(Post.objects.filter(), pk=post_pk)
        serializer.save(author=self.request.user, parent=post)


class CommentToCommentCreateView(generics.CreateAPIView, ):
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        comment_pk = self.kwargs.get('comment_pk')
        comment = get_object_or_404(Comment.objects.filter(), pk=comment_pk)
        serializer.save(author=self.request.user, parent=comment)


class PostCommentListView(generics.ListAPIView):
    serializer_class = PostCommentSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        post_pk = self.kwargs.get('post_pk')
        content_type = ContentType.objects.get_for_model(Post)
        return Comment.objects.filter(parent_id=post_pk, parent_type=content_type)


class PostLikeCreateDestroy(generics.CreateAPIView,
                            generics.DestroyAPIView):
    serializer_class = LikeSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        post_pk = self.kwargs.get('post_pk')
        content_type = ContentType.objects.get_for_model(Post)
        return Like.objects.filter(object_id=post_pk, object_type=content_type, author=self.request.user)

    def get_object(self):
        return get_object_or_404(self.get_queryset())

    def perform_create(self, serializer):
        post_pk = self.kwargs.get('post_pk')
        content_type = ContentType.objects.get_for_model(Post)
        post = get_object_or_404(Post.objects.filter(), pk=post_pk)
        like = Like(object_type=content_type, object_id=post.pk, author=self.request.user)
        like.save()


class CommentLikeCreateDestroy(generics.CreateAPIView,
                               generics.DestroyAPIView):
    serializer_class = LikeSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        comment_pk = self.kwargs.get('comment_pk')
        content_type = ContentType.objects.get_for_model(Comment)
        return Like.objects.filter(object_id=comment_pk, object_type=content_type, author=self.request.user)

    def get_object(self):
        return get_object_or_404(self.get_queryset())

    def perform_create(self, serializer):
        comment_pk = self.kwargs.get('comment_pk')
        content_type = ContentType.objects.get_for_model(Comment)
        comment = get_object_or_404(Comment.objects.filter(), pk=comment_pk)
        like = Like(object_type=content_type, object_id=comment.pk, author=self.request.user)
        like.save()
