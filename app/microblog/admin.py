from django.contrib import admin

from microblog.models import Post, Comment, Media


class PostAdmin(admin.ModelAdmin):
    list_display = ('content', 'author', 'tag_list', 'updated', 'created')
    fields = ('content', 'author', 'article_url')

    def get_queryset(self, request):
        return super(PostAdmin, self).get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join('#{}'.format(o.name) for o in obj.tags.all())


class PostCommentAdmin(admin.ModelAdmin):
    list_display = ('content', 'author', 'tag_list', 'updated', 'created', 'parent_id', 'parent_type')
    fields = ('content', 'author', 'parent_id', 'parent_type')

    def get_queryset(self, request):
        return super(PostCommentAdmin, self).get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join('#{}'.format(o.name) for o in obj.tags.all())


class MediaAdmin(admin.ModelAdmin):
    list_display = ('image', 'parent_id', 'parent_type')
    fields = ('image', 'parent_id', 'parent_type')


admin.site.register(Post, PostAdmin)
admin.site.register(Comment, PostCommentAdmin)
admin.site.register(Media, MediaAdmin)
